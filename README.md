# Api Sicredi: Challenge Final
![Static Badge](https://img.shields.io/badge/maven-3.9.6-8A2BE2) ![Static Badge](https://img.shields.io/badge/java-21.0.2-blue)

Este último challenge tem como desafio final a análise e automação de testes da [api Sicredi](https://github.com/desafios-qa-automacao/desafio-sicredi/tree/master) e é feito juntamente com ferramentas que explorem o software o máximo possível.

## Índice
1. [INSTRUÇÕES](https://gitlab.com/inicio01/api-sicredi#instru%C3%A7%C3%B5es);
2. [AUTOR](https://gitlab.com/inicio01/api-sicredi#autor);
3. [AGRADECIMENTOS](https://gitlab.com/inicio01/api-sicredi#agradecimentos).

## Instruções
Para que você possa obter uma cópia desse projeto em operação localmente em sua máquina, siga as instruções a baixo: 

### Pré-requisitos
- Instalar o Java (versão 21.0.2 em diante);
- Instalar o maven (versão 3.9.6 em diante);
- Configurar o Java e o maven como variáveis de ambiente
- Instalar o Eclipse IDE.

### Instalação

Passo 1: baixar o projeto no github neste [link](https://github.com/desafios-qa-automacao/desafio-sicredi/tree/master), o qual possui formato zip e logo depois extraí-lo para uma pasta de fácil acesso.

Passo 2: abrir o projeto no Eclipse IDE configurado no maven e abrir no terminal e executar mvn install.

Passo 3: executar mvn clean spring-boot:run no terminal, fazendo com que rode localmente como na imagem a seguir.

![](readme.png)
## Autor
- Bruna Praxedes dos Santos: _Documentação_;
- Bruna Praxedes dos Santos: _Automação dos testes_.

## Agradecimentos
- Jessica Elisa de Oliveira Silva;
- Matheus Domingos Locatelli;
- Pedro Raimundo de Carvalho Junior.

